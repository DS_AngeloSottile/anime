<?php 
class Anime extends dbconnect{ //INDICA LE COLONNE DELLA RELATIVA TABELLA E LE SUE FUNZIONI RICHIAMO LE QUERY
    
    //VARAIBILI // variabili classi publiche uguali al nome delle colonne
    public $id;
    public $name;
    public $genere;
    public $cover;
    public $trama;
    public $annoid;
    public $periodoid;
    //VARIABILI
    
    private static $tablename= 'anime';
    
    
    //COSTRUTTORE NOMI DELLE COLONNE
    public function __construct($id,$name,$genere,$cover,$trama,$annoid,$periodoid)
    {
        $this->id = $id;
        $this->name = $name;
        $this->genere = $genere;
        $this->cover = $cover;
        $this->trama = $trama;
        $this->annoid = $annoid;
        $this->periodoid = $periodoid;
    }  
    //COSTRUTTORE
    

    //FUNZION DI RICHIAMO QUEIRY
    public static function create($data)
    {
      
      return parent::insert(self::$tablename,$data);
    }
    
     public static function selectAllAnime()
     {
        $animes= parent::selezionatutto(self::$tablename);
        $vettoreoggetti = Anime::creaoggetti($animes);
        return $vettoreoggetti;
     }
     
     public static function selezionadoveAnime($tablename,$column,$operator,$value,$type)
     { 
        $righetabella = parent::selezionadove($tablename,$column,$operator,$value,$type);
        $vettoreoggetti = Anime::creaoggetti($righetabella);
        return $vettoreoggetti;
     }
     
     public static function selectAnimeAnd($tablename,$column,$column2,$operator,$value,$value2,$type)
     { 
        $righetabella = parent::selezionadoveAND($tablename,$column,$column2,$operator,$value,$value2,$type);
        $vettoreoggetti = Anime::creaoggetti($righetabella);
        return $vettoreoggetti;
     }
     
     	public static function selezionaidAnime($id) // nei metodi statici non puoi utilizzare this, metti self
      {
         $righatabella= parent::selezionaid($id,self::$tablename);
         $vettoreoggetto = Anime::creaoggetto($righatabella);
         return $vettoreoggetto; 
      }
      
      public static function updateanime($id,$array)
      {
         return $righatabella =parent::update(self::$tablename,$array,$id);
      }
      
      public static function deleteanime($array)
      {
         return $righatabella = parent::delete(self::$tablename,$array);
      }  
    //FUNZION DI RICHIAMO QUEIRY
   
   
   //FUNZIONE DI CREAZIONE OGGETTO ALL'INTERNO DELL'OGGETTO STESSO
   public static function creaoggetti($righetabella)
	{
		$vettoreoggetti = [];
		foreach($righetabella as $colonna)
		{
            $vettoreoggetti[]=new self($colonna["id"],$colonna["name"],$colonna["genere"],$colonna["cover"],$colonna["trama"],$colonna["annoid"],$colonna["periodoid"]);
		}
		return $vettoreoggetti;
	}
   
   
   
   public static function creaoggetto($righatabella)
	{
      $oggettoanime=new self($righatabella["id"],$righatabella["name"],$righatabella["genere"],$righatabella["cover"],$righatabella["trama"],$righatabella["annoid"],$righatabella["periodoid"]);
	
		return $oggettoanime;
	}
    //FUNZIONE DI CREAZIONE OGGETTO ALL'INTERNO DELL'OGGETTO STESSO
}

?>