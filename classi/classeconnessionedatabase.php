<?php

class dbconnect { // QUESTA CLASSE FA 2 COSE
                  // 1) CONNETERSI AL DATABSE TRAMITE I 5 PARAMETRI
                  // 2) SCRIVE LE QUERY CHE VERRANNO RICHIAMATE DA TUTTE LE CLASSI(OVVERO LE TABELLE DEL DATBASE)
   
   //VARIABILI //LE 5 VARIABILI PER CONNETTERI AL DATABASE 
   public $host; 
   public $user; 
   public $pwd;
   public $db_main;
   public $port;
   //VARIABILI
   
   
   //VARIABILI SERVILI
   public $connection;
   public $risultatoquery;
   //VARIABILI SERVILI
   
   //COSTRUTTORE FUNZIONE DI ACQUISIRE I PARAMENTRI PER UNA CONNESSIONE E ESEGUIRE LA CONNESSIONE
   public function   __construct()
   {
      $this->host='localhost';
      $this->user='root';
      $this->pwd='';
      $this->db_main='animechart';

      $this->port= 3306;
      
      // CREI UNA CLASSE MYSQLI DALAL QUALE PUOI RICHIAMARE I SUOI METODI
      $this->connection= new mysqli($this->host,$this->user,$this->pwd,$this->db_main,$this->port);
      if($this->connection->connect_errno)
      {
         die('Connessione alla base dati fallita'.$this->connection->connect_error);
      }
      // CREI UNA CLASSE MYSQLI DALAL QUALE PUOI RICHIAMARE I SUOI METODI
      
   }
   //COSTRUTTORE
   
   
   
   
   //SCRIVIAMO LE QUERY
   public function selezionatutto ($tablename)
   {
      
      $db = new dbconnect();
      //SCRIVIAMO LA QUERY
      $query = "SELECT * FROM {$tablename}";
      $risultatoquery =$db->executequery($query);
      //E LA ESEGUIAMO
      
      $vrighetabella = [];
      while($contenutocolonne = mysqli_fetch_array($risultatoquery,MYSQLI_ASSOC))
      {
         $vrighetabella[] = $contenutocolonne;
      }
      return $vrighetabella;
   }
   
   public function selezionadove($tablename,$column,$operator,$value,$type)
   {
      $db = new dbconnect();
      // SELECT * FROM anime WHERE `periodoid`= 1 AND `annoid`= 4;
      // SELECT * FROM Customers WHERE Country='Mexico';
      $query = "SELECT * FROM {$tablename} WHERE {$column} {$operator}";
      if($type==='string')
      {
         $query = $query."'".$value."'";
      }
      elseif($type==='int')
      {
         $query = $query.$value;
      }
      $risultatoquery = $db->executequery($query);
      
      $vrighetabella = [];
      while($contenutocolonne = mysqli_fetch_array($risultatoquery,MYSQLI_ASSOC))
      {
         $vrighetabella[] = $contenutocolonne;

      }
      return $vrighetabella;
   }
   
   public function selezionadoveAND($tablename,$column,$column2,$operator,$value,$value2,$type)
   {
      $db = new dbconnect();
      // SELECT * FROM anime WHERE `periodoid`= 1 AND `annoid`= 4;
      // SELECT * FROM Customers WHERE Country='Mexico';
      $query = "SELECT * FROM {$tablename} WHERE {$column} {$operator}";
      if($type==='string')
      {
         $query = $query."'".$value."'"." AND {$column2} {$operator}"."'".$value2."'";
      }
      elseif($type==='int')
      {
         $query = $query.$value." AND {$column2} {$operator}".$value2;
      }
      $risultatoquery = $db->executequery($query);
      $vrighetabella = [];
      while($contenutocolonne = mysqli_fetch_array($risultatoquery,MYSQLI_ASSOC))
      {
         $vrighetabella[] = $contenutocolonne;  
      }
      return $vrighetabella;
   }
   
    public function   selezionaid($id,$tablename)
    {
       
      $db = new dbconnect();

      $query = "SELECT * FROM {$tablename} WHERE `id`={$id}";
      
      $risultatoquery= $db->executequery($query);
     
     $contenutocolonne = mysqli_fetch_array($risultatoquery,MYSQLI_ASSOC);
     if($contenutocolonne)
     {
      return $contenutocolonne;
     }
     else
     {
      return $s ='non esiste';
     }
   }
   
   public function update($tablename,$array,$id)
   {
      
      $db = new dbconnect();

      // UPDATE table_name
      //SET column1 = value1, column2 = value2, ...
      //WHERE condition;
      $query= 'UPDATE '. $tablename . ' SET ';
      
      $data_size= count($array);
      foreach($array as $d=>$item)
      {
         if($item['type']==='string')
         {
            $query= $query.'`'.$item['column'].'` = \''.$item['value'].'\'';
         }
         else if($item['type']==='int')
         {
            $query= $query.'`'.$item['column'].'` = '.$item['value'];
         }
         if($d<$data_size -1)
         {
            $query=$query.', ';
         }
      }
      $query = $query.' WHERE id='.$id;
      
      return $risultatoquery = $db->executequery($query);
   }
   
   public function delete($tablename,$array)
   {
      
      $db = new dbconnect();

       // DELETE FROM table_name WHERE name = value;
      $query= 'DELETE FROM '. $tablename . ' WHERE ';
      
      $data_size= count($array);
      foreach($array as $d=>$item)
      {
         if($item['type']==='string')
         {
            $query= $query.'`'.$item['column'].'` = \''.$item['value'].'\'';
         }
         else if($item['type']==='int')
         {
            $query= $query.'`'.$item['column'].'` = '.$item['value'];
         }
         
      
       return $risultatoquery = $db->executequery($query);
      }
   }
   
   public function insert( $tableName, $data ) 
   {
      
      $db = new dbconnect();

      
      $query = 'INSERT INTO ' . $tableName;

      $columns = ' (';
      $values = ' (';


      $data_size = count( $data );
      foreach( $data as $d => $item ) 
      {

         $columns .= '`' . $item['column'] . '`';

         if( $item['type'] === 'string' )
         {
            $values .=  '\'' . $item['value'] . '\'';
         } else if( $item['type'] === 'int' ) 
         {
            $values .= $item['value'];
         }
         

         if ( $d < $data_size - 1 ) 
         {
            $columns .= ', ';
            $values .= ', ';
         }
  
      }

      $columns .= ') ';
      $values .= ');';



      $query .= $columns . ' VALUES ' . $values;
      
      $result = $db->executeQuery( $query );

      if( $result ) 
      {
         $db->connection->insert_id; // inserisce lui l'id automaticamente
         return true;
      } 
      else 
      {
         return -1;
      }
  }

   //SCRIVIAMO LE QUERY
   
   
   
   
   
   //FUNZIONI SERVILI
   private function executequery($query)
   {
       
       $risultatoquery = $this->connection->query($query); // si esegue con la funzione query()
       if($risultatoquery)
      {
         return $risultatoquery;
      }
      else
      {
         die('Connessione alla  query fallita'.$this->connection->connect_error);
      }
   }
   //FUNZIONI SERVILI
   
}
?>