<?php

    function print_debug( $var ) {
        echo '<pre>';
        print_r( $var );
        echo '</pre>';
    }
    
    
    function dump_debug( $var ) {
        echo '<pre>';
        var_dump( $var );
        echo '</pre>';
    }
    
    function checklogin($email,$password)
    {
        return User::login($email,$password);
    }
    
    function logout()
    {
        session_unset();
        session_destroy();
        header('location: http://localhost:8080/studiophp\Progetti\Back-End\anime\login.php');
    }
    
    function storesession($field,$data)
    {
        $_SESSION[$field] = $data;
    }
    
        
    function is_auth()
    {
        return isset($_SESSION['user']);
    }
    
    
    function updateanime( $data) {
        $array =[
            [
                'type'=>'string',
                'column'=>'name',
                'value'=>$data['name'] //questo è quello che arriva dal POST e finisce nel valore di value
            ],
            [
                'type'=>'string',
                'column'=>'genere',
                'value'=>$data['genere']
            ],
            [
                'type'=>'string',
                'column'=>'cover',
                'value'=>$data['cover']
            ],
            [
                'type'=>'string',
                'column'=>'trama',
                'value'=>$data['trama']
            ],
            [
                'type'=>'int',
                'column'=>'annoid',
                'value'=>$data['annoid']
            ],
            [
                'type'=>'int',
                'column'=>'periodoid',
                'value'=>$data['periodoid']
            ]
        ];
        
        $updated=Anime::updateanime($data['animeid'],$array);
        if($updated)
        {
            header('location: http://localhost:8080/studiophp\Progetti\Back-End\anime\Winter.php');
            return true;
        }
        else
        {
            return "Modifica andata a malo fine";
        }
    }

    function createAnime( $data) {
        $array =[
            [
                'type'=>'string',
                'column'=>'name',
                'value'=>$data['name'] //questo è quello che arriva dal POST e finisce nel valore di value
            ],
            [
                'type'=>'string',
                'column'=>'genere',
                'value'=>$data['genere']
            ],
            [
                'type'=>'string',
                'column'=>'cover',
                'value'=>$data['cover']
            ],
            [
                'type'=>'string',
                'column'=>'trama',
                'value'=>$data['trama']
            ],
            [
                'type'=>'int',
                'column'=>'annoid',
                'value'=>$data['annoid']
            ],
            [
                'type'=>'int',
                'column'=>'periodoid',
                'value'=>$data['periodoid']
            ]
        ];
        
        $created=Anime::create($array);
        if($created)
        {
            header('location: http://localhost:8080/studiophp\Progetti\Back-End\anime\Winter.php');
            return true;
        }
        else
        {
            return "Creazione andata a malo fine";
        }
    }
    
    function deleteanime($data) {
        $array =[
            [
                'type'=>'string',
                'column'=>'id',
                'value'=>$data['id'] 
            ]
        ];
        
        $deleted=Anime::deleteanime($array);
        if($deleted)
        {
            $s="Anime eliminato con successo";
            return $s;
        }
        else
        {
            return "Eliminazione andata a malo fine";
        }
    }

?>