<!DOCTYPE html>
<html>
	<?php
		//PER COMINCIARE, AVRAI SEMPRE BISOGNO DI UN MINIMO DI 2 CLASSI
		//UAN DI ESSE SI CONNETTE AL DATABASE, E RICHIAMA UNA CLASSE  CON I METODI PER FAR ESEGUIRE LE QUERY
		//PER QUESTO E' BUONO SCRIVERE LE QUERY SOLO SULLA CLASSE CHE SI CONENTTE AL DATABASE E POI RICHIAMARLE NELLE ALTRE CLASSI
		// SE NO DEVI RICHIAMARE GLOBALMENTE LA L'OGGETTO CON AL CONNESSIONE AL DATABSE
	?>
<head>
	<?php
	 $annocorrente = 4;
	//FILE IMPORTATI
	require_once './fileservili/init.php';
	//FILE IMPORTATI
	if(isset($_SESSION['user']))
	{
	?>
	<title>Anime main page</title>
</head>

<body>
	<?php include_once './partials/menu.php' ?>
<main>

<?php
$anime=Anime::selezionadoveAND('anime','periodoid','annoid','=',4,$annocorrente,'int');



foreach($anime as $animesingolo)
{ ?>
	<article class="boxanime">
		<div class="boxtitolo"><div class="cv"><?php echo $animesingolo->name?></div></div>
		<div class="boximg"><img style=" width: 100%; height:100%;" src="<?php echo $animesingolo->cover ?>"></div>
		<div class="boxinfo">
			<div class="boxgenere"><div class="cv"><b><?php echo $animesingolo->genere?></b></div></div>
			<div class="boxtrama"><b><?php echo $animesingolo->trama?></b></div>
		</div>
		<?php if($_SESSION['user']->ruoloid == 2)
			{ ?>
				<div class="boxcollegamenti"><a href="amministrazione.php?id=<?php echo $animesingolo->id ?>">Dettagli Anime</a></div> <?php } ?>
		</article>
	<?php } ?>



<?php
}
else
{
	header('location: http://localhost:8080//studiophp/Mie%20creazioni/anime/login.php');
}
?>
</main>
</body>
</html>
