<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<?php

require_once './fileservili/init.php'; ?>
	<title>Untitled</title>
</head>

<body>
<?php if(isset($_SESSION['user']))
	{
		if($_SESSION['user']->ruoloid == 1)
		{
			header('location: http://localhost:8080//studiophp/Miecreazioni/anime/winter.php');
		}

if(isset($_GET['id']))
	{
		$animesingolo =  Anime::selezionaidAnime($_GET['id']); ?>
		<article class="boxanime">
			<div class="boxtitolo"><div class="cv"><?php echo $animesingolo->name?></div></div>
			<div class="boximg"><img style=" width: 100%; height:100%;" src="<?php echo $animesingolo->cover ?>"></div>
			<div class="boxinfo">
				<div class="boxgenere"><div class="cv"><b><?php echo $animesingolo->genere?></b></div></div>
				<div class="boxtrama"><b><?php echo $animesingolo->trama?></b></div>
			</div>
			<div class="boxcollegamenti"><a href="amministrazione.php?id=<?php echo $animesingolo->id ?>">Dettagli Anime</a></div>
		</article>
	
		<div class="divform">


			<div class="modifica">
				<p> Modifica Anime </p>
				<form action="" method="post">
					<div>
						Nome	
					</div>
					<div>
						<input type="tex" name="name" value="<?php echo $animesingolo->name?>">
					</div>
					
					<div>
						Genere	
					</div>
					<div>
						<input type="tex" name="genere" value="<?php echo $animesingolo->genere?>">
					</div>
					
					
					<div>
						Cover	
					</div>
					<div>
						<input type="tex" name="cover" value="<?php echo $animesingolo->cover?>">
					</div>
					
					
					<div>
						Trama
					</div>
					<div>
						<input type="text" name="trama" value="<?php echo $animesingolo->trama?>">
					</div>
					
					
					<div>
						Annoid
					</div>
					<div>
						<input type="text" name="annoid" value="<?php echo $animesingolo->annoid?>">
					</div>
					
					
					<div>
						Periodoid
					</div>
					<div>
						<input type="text" name="periodoid" value="<?php echo $animesingolo->periodoid?>">
					</div>
				
				
					<div>
						<input type="hidden" name="animeid" value="<?php echo $animesingolo->id ?>">
						<button type="submit" name="edit" >Salva modifiche</button>
					</div>
				</form>
			</div>


	
			<div class="modifica">
				<p> Elimina Anime </p>
				<form action="" method="post">
					<div>
						<input type="hidden" name="id" value="<?php echo $animesingolo->id?>">
					</div>
					<div>
						<button type="submit" name="delete" >Elimina</button>
					</div>
				</form>
			</div>
	
	

			<div class="divform">
				<div class="modifica">
					<p> Aggiungi Anime </p>
					<form action="" method="post">
						<div>
							Nome	
						</div>
						<div>
							<input type="tex" name="name" value="">
						</div>
						
						<div>
							Genere	
						</div>
						<div>
							<input type="tex" name="genere" value="">
						</div>
						
						
						<div>
							Cover	
						</div>
						<div>
							<input type="tex" name="cover" value="">
						</div>
						
						
						<div>
							Trama
						</div>
						<div>
							<input type="text" name="trama" value="">
						</div>
						
						
						<div>
							Annoid
						</div>
						<div>
							<input type="text" name="annoid" value="">
						</div>
						
						
						<div>
							Periodoid
						</div>
						<div>
							<input type="text" name="periodoid" value="">
						</div>
					
					
						<div>
							<button type="submit" name="Add" >Crea Anime</button>
						</div>
					</form>
				</div>
			</div> 



		</div>


<?php
	}
else
{
	$messaggio ="Nessun anime selezionato";
	echo $messaggio;
}
?>
	
	
<?php
	if(isset($_POST['delete']))
	{
		$risultato = deleteanime($_POST);
		$_SESSION['messaggio'] = $risultato;
		header('location: http://localhost:8080/studiophp\Progetti\Back-End\anime\Winter.php');
	} 

	if(isset($_POST['Add']))
	{
		$result = createAnime($_POST);
	} 
?>
<div>
	<a href="./Winter.php">Vai alla pagina principale</a>
</div>

	<div class="mainbox">
		<?php
		if(isset($_POST['edit']))
		{
			$risultato = updateanime($_POST);
			$newanime=Anime::selezionaidAnime($_GET['id']); ?>
		<div class="info">
			Anime modificato
		</div>
		<article class="boxanime">
			<div class="boxtitolo"><div class="cv"><?php echo $newanime->name?></div></div>
			<div class="boximg"><img style=" width: 100%; height:100%;" src="<?php echo $newanime->cover ?>"></div>
			<div class="boxinfo">
				<div class="boxgenere"><div class="cv"><b><?php echo $newanime->genere?></b></div></div>
				<div class="boxtrama"><b><?php echo $newanime->trama?></b></div>
			</div>
			<div class="boxcollegamenti"><a href="amministrazione.php?id=<?php echo $newanime->id ?>">Dettagli Anime</a></div>
		</article>
	</div>
	<?php }
	}
else
{
	header('location: http://localhost:8080/studiophp\Progetti\Back-End\anime\login.php');
}
?>
	

	


</body>
</html>
