<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<?php require_once './fileservili/init.php'; ?>
	<title>Untitled</title>
</head>

<body>
<?php
if(isset($_GET['nameanno']) && isset($_GET['nameperiodo']))
{ ?>
<h1> Benvenuto ai cartoni animati Anno : <?php echo $_GET['nameanno'] ?>  e periodo : <?php echo $_GET['nameperiodo'] ?>  </h1>
<?php
 include_once './partials/menu.php';
}
if(isset($_GET['idanno']) && isset($_GET['idperiodo']))
	{
		$anime=Anime::selezionadoveAND('anime','periodoid','annoid','=',$_GET['idperiodo'],$_GET['idanno'],'int');
		foreach($anime as $animesingolo)
		{ ?>
			<article class="boxanime">
				<div class="boxtitolo"><div class="cv"><?php echo $animesingolo->name?></div></div>
				<div class="boximg"><img style=" width: 100%; height:100%;" src="<?php echo $animesingolo->cover ?>"></div>
				<div class="boxinfo">
					<div class="boxgenere"><div class="cv"><b><?php echo $animesingolo->genere?></b></div></div>
					<div class="boxtrama"><b><?php echo $animesingolo->trama?></b></div>
				</div>
				<?php if($_SESSION['user']->ruoloid == 2)
					{ ?>
						<div class="boxcollegamenti"><a href="amministrazione.php?id=<?php echo $animesingolo->id ?>">Dettagli Anime</a></div> <?php } ?>
			</article>
	<?php } ?>
<?php
	}
?>

</body>
</html>
